#pragma once 

/*
 * C++では、#defineディレクティブは使用すべきでない。
 * inlineとconstやconstexprで置き換えられる部分は置き換えるべき(戒め)
 */

// LEDの点灯制御マクロ
inline void TURN_ON(int led)
{
	PORTB |= led;
}
inline void TURN_OFF(int led)
{
	PORTB &= ~led;
}

// 各種定数の設定
const double DECREASE_PRESSURE	= 0.7;
const double INCREASE_PRESSURE	= 3.0;
const int PRESSURE_TIME			= 20;
const int TRIAL_LIMIT			= 20;
const int AD_RESOLUTION			= 1024;
const double AD_REFERENCE		= 3.3;

// ピンのマッピング
const int START_BUTTON			= 0x04;
const int STOP_BUTTON			= 0x08;
const int END_BUTTON			= 0x10;
const int RELAY 				= 0x7a;
const int PRESSURE_DATA_PIN 	= 0x01;
const int GREEN_LED				= 0x20;
const int YELLOW_LED			= 0x40;
