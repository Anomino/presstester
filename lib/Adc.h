/**********************************
 * AVRのADCペリフェラルライブラリ *
 **********************************/

//#pragmaディレクティブによるインクルードガード
#pragma once

#include <avr/io.h>
#include <avr/interrupt.h>
#include "Adc_config.h"

//ADCペリフェラルの初期化
void InitAdc();
void StartADC(int adc_ch);
uint16_t ReceiveAD();
