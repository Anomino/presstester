#include "DriveDisplay.hpp"

bool InitDisplay()
{
	_delay_ms(15);

	if(SendCommandDisplay(DISPLAY_CLEAR) == true)
	{
		_delay_ms(5);

		if(SendCommandDisplay(DISPLAY_ON_OFF) == true)
		{
			_delay_ms(5);

			if(SendCommandDisplay(DISPLAY_FUNCTION_SET) == true)
			{
				_delay_ms(5);
				return true;
			}
			else
			{

			}

		}
		else
		{

		}
	}
	else
	{

	}
}

bool SendCommandDisplay(uint8_t command)
{
	if(StartI2C(0xc0) == true)
	{
		WriteData(0x00);
		WriteData(command);

		StopI2C();

		return true;
	}
	else
	{
		return false;
	}
}

bool SendDataDisplay(uint8_t data)
{
	if(StartI2C(ADDRESS | WRITE) == true)
	{
		WriteData(0x80);
		WriteData(data);

		StopI2C();

		return true;
	}
	else
	{
		return false;
	}
}

bool SendTextDisplay(char *text)
{
	const int text_length = sizeof(text) / sizeof(text[0]);

	for(int loop = 0; loop < text_length; loop ++)
	{
		if(SendDataDisplay(text[loop]) == false)
		{
			return false;
		}
	}

	return true;
}

// 接続チェック
/*
int main()
{
	InitI2C(100000);
	DDRB = 0xff;

	if(InitDisplay() == true)
	{
		PORTB |= 0x01;
	}

	while(1)
	{
		//SendTextDisplay("Hello");
		SendDataDisplay('B');
	}

}
*/
