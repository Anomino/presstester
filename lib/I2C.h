/**********************************
 * AVRのI2Cペリフェラルライブラリ *
 **********************************/

#pragma once

#include <avr/io.h>
#include <avr/interrupt.h>
#include "I2C_Status.h"

#define WAIT_COMP while(!(TWCR & (1 << TWINT)))

void InitI2C(uint32_t twiFreq);		//TWI機能の初期化
bool StartI2C(uint8_t address);		//開始条件からデバイス指定まで
void StopI2C();						//終了条件送出
bool WriteData(uint8_t data);					//バスに書き込み
uint8_t ReceiveAck();				//ACKを返す。
uint8_t ReceiveNack();				//NACKを返す。

inline void AddressSet(uint8_t address)
{
	TWAR = address;
	TWCR = (1 << TWEN) | (1 << TWEA);
	WAIT_COMP;
}
