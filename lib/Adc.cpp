/**********************************
 * AVRのADCペリフェラルライブラリ *
 **********************************/

#include "Adc.h"

void InitAdc()
{
	ADMUX  |= (_REF_VOL_ << 6);
	ADCSRA |= 0x88 | (_ADC_PRS_);
}

void StartADC(int adc_ch)
{
	ADMUX |= adc_ch;
	ADCSRA |= (1 << 6);
}

uint16_t ReceiveAD()
{
	while(ADCSRA & 0x10);

	return ADCH | ADCL;
}
