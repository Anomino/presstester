#pragma once

#define TWI_START			0x08
#define TWI_RESTART			0x10
#define TWI_MT_SLA_ACK		0x18
#define TWI_MR_SLA_ACK		0x40
#define TWI_MT_DATA_ACK		0x28
#define TWI_ST_DATA_ACK		0xb8
#define TWI_ST_DATA_NACK	0xc0
