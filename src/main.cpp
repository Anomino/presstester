#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include "../lib/Adc.h"
#include "../lib/DriveDisplay.hpp"
#include "PressTester_config.hpp"

static constexpr int PRESSURE_TIME_COUNT = 1000 / PRESSURE_TIME;
static uint8_t pressure_count = 0;		// 加圧時間カウント
static uint8_t trial_count = 0;			// 試行回数カウント

using namespace std;

// configファイルから定数を持ってくる。
extern const double DECREASE_PRESSURE;
extern const double INCREASE_PRESSURE;
extern const int PRESSURE_TIME;
extern const int TRIAL_LIMIT;

extern const int START_BUTTON;
extern const int STOP_BUTTON;
extern const int END_BUTTON;
extern const int RELAY;
extern const int PRESSURE_DATA_PIN;
extern const int GREEN_LED;
extern const int YELLOW_LED;
extern const int AD_RESOLUTION;
extern const double AD_REFERENCE;

enum Status
{
	Start = 0,
	Stop = 1,
	End = 2,
	Decrease = 3,
};

static Status status = Stop;

bool InitMain()
{
	DDRB = 0xff;	// 確認用LED
	DDRD = 0xe0;	// FET＆各種プラケットLEDを駆動　＆　スイッチの入力

	InitAdc();
	InitI2C(100000);

	if(InitDisplay() == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

Status CheckButton()
{
	// ボタンのチェック
	switch(PIND & (START_BUTTON | STOP_BUTTON | END_BUTTON))
	{
		case START_BUTTON : 
			return Start;
			break;

		case STOP_BUTTON : 
			return Stop;
			break;

		case END_BUTTON : 
			return End;
			break;

		default : 
			return Stop;
			break;
	}
}

inline void DriveRelay()
{
	// 電磁弁の開放
	PORTD |= RELAY;
}

inline void CloseRelay()
{
	// 電磁弁閉じる
	PORTD &= ~RELAY;
}

inline void OnGreenLed()
{
	PORTD |= GREEN_LED;
}

inline void OffGreenLed()
{
	PORTD &= ~GREEN_LED;
}

inline void OnYellowLed()
{
	PORTD |= YELLOW_LED;
}

inline void OffYellowLed()
{
	PORTD &= ~YELLOW_LED;
}

double GetPressureValue()
{
	// 気圧値の取得
	//StartADC(PRESSURE_DATA_PIN);
	StartADC(0x08);

	// 気圧は、センサから取得した1-5Vスケールのアナログ電圧を
	// 抵抗分圧によって0.64-3.3Vにスケーリングした電圧をコンバートする。
	
	const double pressure = ReceiveAD() / AD_RESOLUTION * AD_REFERENCE;
	return pressure;

}

void Wait()
{
	_delay_ms(PRESSURE_TIME);
}

void ShowStatusDisplay(Status status, double pressure, int trial, int time)
{
	static char display_line[2][16];

	sprintf(display_line[0], "%d", pressure);

	switch(status)
	{
		case Start : 
			SendTextDisplay("MODE : START ");
			break;
		case Stop : 
			SendTextDisplay("MODE : STOP  ");
			break;
		case End : 
			SendTextDisplay("MODE : END   ");
			break;
		case Decrease : 
			SendTextDisplay("MODE : DECR ");
			break;
		default : 
			break;
	}

	SendCommandDisplay(DISPLAY_INDENTION);

	SendTextDisplay(display_line[0]);
	SendCommandDisplay(DISPLAY_START);
}

int main()
{

	if(InitMain() == true)
	{
		TURN_ON(0x01);
	}
	else
	{
		TURN_ON(0b10101010);
		while(1);
	}

	while(1)
	{
		
		// 気圧取得
		double pressure = GetPressureValue();

		// ボタン状態チェック
		if(status != Decrease)
		{
			// 減圧状態でなければステータス更新
			status = CheckButton();
		}
		else
		{

		}

		ShowStatusDisplay(status, pressure, trial_count, pressure_count * PRESSURE_TIME);

		switch(status)
		{
			case Start : // 開始フラグ
				TURN_ON(0x02);
				TURN_OFF(0x1c);
				DriveRelay();
				break;

			case Stop : // 一時停止フラグ
				TURN_ON(0x04);
				TURN_OFF(0x1a);
				CloseRelay();
				break;

			case End : // 終了フラグ
				TURN_ON(0x08);
				TURN_OFF(0x16);
				CloseRelay();
				break;

			case Decrease : // 減圧フラグ
				TURN_ON(0x10);
				TURN_OFF(0x0e);
				CloseRelay();
				break;

			default : 
				break;
		}
	
		if(pressure >= INCREASE_PRESSURE)
		{
			// 目標気圧に到達
			TURN_ON(0x20);

			if(status == Start)
			{
				// 開始ステータス
				TURN_ON(0x40);
				Wait();
				pressure_count ++;
				TURN_OFF(0x40);

				if(pressure_count >= PRESSURE_TIME_COUNT)
				{
					// 一定時間ボトルに加圧し続けた
					status = Decrease;				
					pressure_count = 0;
				}
				else
				{
					// 加圧中
				
				}
			}
			else
			{
				// 目標気圧に到達したが、ステータスが開始でない。
				// 通常通らない部分
			}
		}
		else
		{
			if(status == Decrease)
			{
				// 減圧
				
				if(pressure <= DECREASE_PRESSURE)
				{
					// 減圧完了
					trial_count ++;
					status = Start;
				}
				else
				{
					// 減圧中
				}
			}
			else
			{
				// 減圧ステータスでない
			}
		}

		if(trial_count >= TRIAL_LIMIT)
		{
			// 目標試行回数に到達
			status = End;
		}
		else
		{
			// 試行中
		}
	
	}

	return 0;
}
